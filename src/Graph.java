import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Graph {
    //map for storing the node as key(node1-node2) and value as distance
    Map<String, Integer> graph2 = new LinkedHashMap<>();
    //map for storing the graph
    Map<String, List<Node>> graph = new HashMap<>();

    public Graph() {
        Scanner sc;
        try {
            //this block will read the data from the city.txt file line by line
            sc = new Scanner(new File("src/graph.txt"));
            while (sc.hasNextLine()) {
                String[] nodes = sc.nextLine().split(", ");
                //loop to traverse file data
                for (String city : nodes) {
                    String key = city.charAt(0) + "-" + city.charAt(1);
                    graph2.put(key, Character.getNumericValue(city.charAt(2)));
                    Node edge = new Node(city.charAt(1) + "", Character.getNumericValue(city.charAt(2)));
                    List<Node> list;
                    if (graph.containsKey("" + city.charAt(0))) {
                        list = graph.get("" + city.charAt(0));
                    } else {
                        list = new ArrayList<>();
                    }
                    list.add(edge);
                    graph.put("" + city.charAt(0), list);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    //this method will return the size of the path having stop less than or equal to given stops
    public int getPathWithMaximumStops(String startPoint, String endPoint, int stops) {
        return getPaths(startPoint, endPoint, stops, startPoint, new ArrayList<>(), "maximum").size();
    }

    //this method will return the size of the path having stop equal to given stops
    public int getPathWithExactStops(String startPoint, String endPoint, int stops) {
        return getPaths(startPoint, endPoint, stops, startPoint, new ArrayList<>(), "equal").size();
    }

    //this method will return the size of the path having total distance less than given distance
    public int getPathWithDistanceLessThan(String startPoint, String endPoint, int distance) {
        return getPathWithDistance(startPoint, endPoint, distance, startPoint, new ArrayList<>()).size();
    }

    //this is a recursive method to find the path between startPoint and endPoint
    public List<String> getPaths(String startPoint, String endPoint, int stops, String newPath, List<String> path, String flag) {
        if (stops == 0 ) {
            return path;
        }
        for (Node node : graph.get(startPoint)) {
            newPath = newPath + "-" + node.getEndPoint();
            switch (flag) {
                case "maximum": {
                    if (node.getEndPoint().equals(endPoint)) {
                        path.add(newPath);
                    } else {
                        this.getPaths(node.getEndPoint(), endPoint, stops - 1, newPath, path, flag);
                    }
                    break;
                }
                case "equal": {
                    if (node.getEndPoint().equals(endPoint) && stops == 1) {
                        path.add(newPath);
                    } else {
                        this.getPaths(node.getEndPoint(), endPoint, stops - 1, newPath, path, flag);
                    }
                    break;
                }
            }
        }
        return path;
    }

    //this is a recursive method to find the path having distance less than the minimum distance
    private List<String> getPathWithDistance(String startPoint, String endPoint, int distance, String newPath, List<String> path) {
        if (distance < 0) return path;
        for (Node edge : graph.get(startPoint)) {
            newPath = newPath + "-" + edge.getEndPoint();
            if (edge.getEndPoint().equals(endPoint) && distance > edge.getDistance()) {
                path.add(newPath);
            }
            this.getPathWithDistance(edge.getEndPoint(), endPoint, distance - edge.getDistance(), newPath, path);
        }
        return path;
    }

    //this method return the length of the minimum path between two given nodes
    public int getLengthOfMinimumPath(String startPoint, String endPoint) {
        Map<String, Integer> distanceOfNodes = new HashMap<>();
        graph.keySet().forEach(node -> distanceOfNodes.put(node, Integer.MAX_VALUE));
        Stack<String> stack = new Stack<>();
        for (Node node : graph.get(startPoint)) {
            distanceOfNodes.put(node.getEndPoint(), node.getDistance());
            stack.push(node.getEndPoint());
        }
        while (!stack.isEmpty()) {
            String nextPoint = stack.pop();
            for (Node node : graph.get(nextPoint)) {
                int len = node.getDistance() + distanceOfNodes.get(nextPoint);
                if (distanceOfNodes.get(node.getEndPoint()) > len) {
                    distanceOfNodes.put(node.getEndPoint(), len);
                    stack.push(node.getEndPoint());
                }
            }
        }
        int shortestPath = distanceOfNodes.get(endPoint);
        return shortestPath;
    }

    //this method return the distance of the path if exists or return "NO SUCH ROUTE"
    public String getDistance(String path) {
        int distance = 0;
        String[] cities = path.split("-");
        for (int i = 0; i < cities.length - 1; i++) {
            String key = cities[i] + "-" + cities[i + 1];
            if (graph2.containsKey(key))
                distance += graph2.get(key);
            else
                return "NO SUCH ROUTE";
        }
        return String.valueOf(distance);
    }
}
