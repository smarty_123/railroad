public class Railroad {
    public static void main(String[] args) {
        Graph graph = new Graph();
        System.out.println("Output 01 : " + graph.getDistance("A-B-C"));
        System.out.println("Output 02 : " + graph.getDistance("A-D"));
        System.out.println("Output 03 : " + graph.getDistance("A-D-C"));
        System.out.println("Output 04 : " + graph.getDistance("A-E-B-C-D"));
        System.out.println("Output 05 : " + graph.getDistance("A-E-D"));
        System.out.println("Output 06 : " + graph.getPathWithMaximumStops("C", "C", 3));
        System.out.println("Output 07 : " + graph.getPathWithExactStops("A", "C", 4));
        System.out.println("Output 08 : " + graph.getLengthOfMinimumPath("A", "C"));
        System.out.println("Output 09 : " + graph.getLengthOfMinimumPath("B", "B"));
        System.out.println("Output 10 : " + graph.getPathWithDistanceLessThan("C", "C", 30));
    }
}
