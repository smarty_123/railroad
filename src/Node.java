//model class for storing the graph data(endPoint and path distance)
public class Node {
    private String endPoint;
    private int distance;

    public Node(String endPoint, int distance) {
        this.endPoint = endPoint;
        this.distance = distance;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
