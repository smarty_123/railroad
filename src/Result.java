//import com.google.gson.Gson;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.net.URL;
//import java.util.List;
//
//class Result {
//    /*
//     * Complete the 'numDevices' function below.
//     *
//     * The function is expected to return an INTEGER.
//     * The function accepts following parameters:
//     *  1. STRING statusQuery
//     *  2. INTEGER threshold
//     *  3. STRING dateStr
//     */
//    static class Response
//    {
//        private int total_pages;
//        private List<Data> data;
//        public Response(){}
//        public Response(int total_pages,List<Data> data)
//        {
//            this.data = data;
//            this.total_pages=total_pages;
//        }
//        public int getTotalPages()
//        {
//            return total_pages;
//        }
//        public List<Data> getData()
//        {
//            return data;
//        }
//
//    }
//    static class Data
//    {
//        private String status;
//        private String timestamp;
//        private OperatingParam operatingParams;
//        public Data(String status, String timestamp, OperatingParam p)
//        {
//            this.status=status;
//            this.timestamp=timestamp;
//            this.operatingParams=p;
//        }
//        public String getStatus()
//        {
//            return status;
//        }
//        public String getTimestamp()
//        {
//            return timestamp;
//        }
//        public OperatingParam getOperatingParam()
//        {
//            return operatingParams;
//        }
//    }
//    static class OperatingParam
//    {
//        private double rootThreshold;
//        public OperatingParam(double rootThreshold)
//        {
//            this.rootThreshold = rootThreshold;
//        }
//        public double getThreshold()
//        {
//            return rootThreshold;
//        }
//    }
//    public static int numDevices(String statusQuery, int threshold, String dateStr) {
//        int total_pages=1;
//        double threshold1 = 0.0+threshold;
//        int count=0;
//        try{
//            for(int i=1;i<=total_pages;i++) {
//                String urlString = "https://jsonmock.hackerrank.com/api/iot_devices/search?status=" + statusQuery + "&page=" + i;
//                URL url = new URL(urlString);
//                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
//                String line;
//                while ((line = br.readLine()) != null) {
//                    Response json = new Gson().fromJson(line, Response.class);
//                    total_pages = json.getTotalPages();
//                }
//            }
//        }